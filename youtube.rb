require 'yt'
require 'csv'

require "google/apis/sheets_v4"
require "googleauth"
require "googleauth/stores/file_token_store"
require "fileutils"


OOB_URI = "urn:ietf:wg:oauth:2.0:oob".freeze
APPLICATION_NAME = "GitLab DE Team Dashboard".freeze
CREDENTIALS_PATH = "credentials.json".freeze
TOKEN_PATH = "token.yaml".freeze
SCOPE = Google::Apis::SheetsV4::AUTH_SPREADSHEETS


def checkExistingVideo(existingVideoIDs, video_id)
  if existingVideoIDs.values.nil?
    return false
  else
    if existingVideoIDs.values.empty?
      return false
    else
      existingVideoIDs.values.each do |row|
        if row[0] == video_id
          return true
        end
      end
      return false
    end
  end
end

# Initialize the APIs
service = Google::Apis::SheetsV4::SheetsService.new
service.client_options.application_name = APPLICATION_NAME
service.authorization = Google::Auth::ServiceAccountCredentials.make_creds(
    json_key_io: File.open('credentials.json'),
    scope: Google::Apis::SheetsV4::AUTH_SPREADSHEETS) 
spreadsheet_id = ENV['VIDEO_ASSETS_SHEETID']
range = "YouTube videos!A2:F"
VALUE_INPUT_OPTION = 'RAW'

Yt.configure do |config|
  config.log_level = :debug
  config.api_key = ENV['GOOGLE_API_KEY']
end

Yt.configuration.api_key = ENV['GOOGLE_API_KEY']

csvData = Array.new

existingVideoIDs = service.get_spreadsheet_values spreadsheet_id, "YouTube videos!A2:B"


File.foreach( 'data/youtube_playlists.txt' ) do |playlist_id_line|
    playlist_id, team, playlist_name = playlist_id_line.split(";")
    playlist = Yt::Playlist.new id: playlist_id.strip
    videoList = playlist.playlist_items

    videoList.each do |video|
        if checkExistingVideo(existingVideoIDs, video.video_id) == false
          csvData.push([video.video_id, team, video.channel_title, playlist.title, "https://www.youtube.com/watch?v="+video.video_id, video.title ])
        end
    end
end

value_range_object = Google::Apis::SheetsV4::ValueRange.new(values: csvData)
service.append_spreadsheet_value(spreadsheet_id, range, value_range_object, value_input_option: VALUE_INPUT_OPTION)
