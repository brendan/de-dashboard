require 'json'
require 'jsonpath'
require 'faraday'
require 'date'

#require 'dotenv'
#Dotenv.load

module Dashboard
    @@chartLabels = ["Jan-2021", "Feb-2021", "Mar-2021",  "Apr-2021", "May-2021", "Jun-2021", "Jul-2021", "Aug-2021", "Sep-2021", "Oct-2021", "Nov-2021", "Dec-2021"]
    @@chartData = [0, 0, 0, 0, 0]
    @@teLabels = {
        "Requests" => ["DE-Request::New","DE-Request::Open","DE-Request::Done","DE-Request::Cancelled"],
        "Content" => ["DE-Content::new-request","DE-Content::draft","DE-Content::review","DE-Content::published","DE-Content::Repurposing","DE-Content::cancelled"],
        "CFP-META" => ["DE-CFP-Upcoming","DE-CFP-Meta::Open","DE-CFP-Meta::Closed", "DE-CFP-Meta::Cancelled"],
        "CFP" => ["DE-CFP::Draft","DE-CFP::Review","DE-CFP::Submitted","DE-CFP::Accepted","DE-CFP::Missed","DE-CFP::Rejected"],
        "Themes" => ["DE-Dev","DE-K8s","DE-Ops"],
        "Process" => ["DE-DueSoon","DE-Process::FYI","DE-Process::Open","DE-Process::Pending","DE-Peer-Review","DE-Process::Done"],
        "Request-Type" => ["DE-Request-Type::External","DE-Request-Type::Internal"],
        "Tasks" => ["DE-Task::CFP","DE-Task::coaching","DE-Task::community","DE-Task::content","DE-Task::event","DE-Task::others","DE-Task::media","DE-Task::process","DE-Task::speaking","DE-Task::webcast"]
    }

    def getIssues(label, extraArgs = '')
        jsonData = Array.new
        if (jsonData.count == 0)
            page = 1
            response = getIssuesEndpoint(page, label, extraArgs)
            headers = response.headers
            totalIssuesPages = headers['X-Total-Pages']
            currentIssuePage = headers['X-Page']
            jsonData.concat(JSON.parse(response.body).to_a)

            while page <= headers['X-Total-Pages'].to_i do
                page += 1
                response = getIssuesEndpoint(page, label, extraArgs)
                headers = response.headers
            
                jsonData.concat(JSON.parse(response.body))
                
            end
        end
        jsonData
    end

    def getIssuesEndpoint(page, label, extraArgs)
        
        Faraday.new(ENV['GITLAB_API'] + 'groups/'+ ENV['GROUP_ID'] + '/issues?labels=' + label + extraArgs + '&per_page=100&page=' + page.to_s, headers: { 'PRIVATE-TOKEN' => ENV['PRIV_KEY'] }).get
        
    end

    def lastWeek
        labels = Array.new
        last_week = Date.today - 7
        data = getIssues('dev-evangelism', '&created_after='+last_week.rfc3339())
        if data.count > 0
            parsedData = JSON.parse(data.to_json)
            parsedData.each {|issue| labels.concat(issue['labels']) }
            #talliedLabels = labels.tally
            talliedLabels = Hash.new
            labels.each { |name| talliedLabels[name] = talliedLabels[name].to_i + 1 }
            
            formattedLabels = '<h5> Issues Created with <em>dev-evangelism</em> label:  ' + data.count.to_s + ' </h5><p>'
            talliedLabels.each do |key, value|
                formattedLabels += '<span style="5px" class="badge badge-light">' + key + ' <span class="badge badge-dark">' + value.to_s + '</span></span>'
            end

            formattedLabels += '</p>'
            formattedLabels
        end

    end
    

    def getTotalIssues
        data = getIssues('dev-evangelism')
        data.count
    end

    def totalOpenIssues
        data = getIssues('dev-evangelism', '&state=opened')
        data.count
    end

    def totalClosedIssues
        data = getIssues('dev-evangelism', '&state=closed')
        data.count
    end

    def processMonthlyStats(data, state)
        issueDates  = Array.new
        parsedData = JSON.parse(data.to_json)
        if state == 1 
            parsedData.each {|issue| issueDates.push(Date.parse(Date.rfc3339(issue["created_at"]).to_s).strftime('%b-%Y')) }
        else
            parsedData.each {|issue| issueDates.push(Date.parse(Date.rfc3339(issue["closed_at"]).to_s).strftime('%b-%Y')) }
        end
        
        
        i = 0

        while i < @@chartLabels.length
            #@@chartLabels.index(Date.rfc3339(issueDate.to_s).strftime('%b-%Y').to_s)
                if state == 1 
                    @@chartData[i] = issueDates.count(@@chartLabels[i])
                else
                    @@chartData[i] = issueDates.count(@@chartLabels[i])
                end
                i += 1
        end
        @@chartData
        #data.count
    end

    def getChartKeys(state = 1)
        @@chartLabels
    end

    def getChartValues(state)
        
        if state == 1 
            #state
            processMonthlyStats(getIssues('dev-evangelism', '&state=opened'), 1)
        else
            #state
            processMonthlyStats(getIssues('dev-evangelism', '&state=closed'), 0)
        end
    end

    def traverseIssuesByLabels
        formattedData = ''
        @@teLabels.each do |key, labels|
            formattedData_Count = ""
            categoryTotal = 0
            labels.each do |label|
                data = getIssues(label)
                formattedData_Count += dataFormat( label , data.count.to_s)
                categoryTotal += data.count
            end
            formattedData_Categories = "<h3>" + key + " <span class='badge badge-dark'>" + categoryTotal.to_s + "</span></h3>"
            formattedData += formattedData_Categories + "<div class='row'>" + formattedData_Count + " </div> <hr/>"
        end
        formattedData
    end


    def dataFormat(label, count)
        '<div class="card bg-light mb-2" style="max-width: 18rem; margin: 10px">
        <div class="card-header">' + label + '</div>
        <div class="card-body" style="DExt-align:center">
          <h5 class="card-title"><a href="https://gitlab.com/groups/gitlab-com/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=' + label + '" target="_blank">' + count + '</a></h5>
          <p class="card-text"></p>
        </div>
      </div>'

    end

    def getUpdatedTime
        Time.now.utc
    end
    
end
